package application.databaseMigrator

import application.databaseMigrator.Migrator
import org.gradle.api.tasks.TaskAction
import org.flywaydb.core.internal.info.MigrationInfoDumper;

public class Info extends Migrator {
    @TaskAction
    public void run() {
        def info_all = initMigrator().info().all()
        logger.quiet('Schema Info\n{}', MigrationInfoDumper.dumpToAsciiTable(info_all))
    }
}
