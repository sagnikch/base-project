package application.databaseMigrator

import application.databaseMigrator.Migrator
import org.gradle.api.tasks.TaskAction

public class Baseline extends Migrator {
    @TaskAction
    public void run() {
        initMigrator().baseline()
        logger.quiet('Schema Baselined !')
    }
}
