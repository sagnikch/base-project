package application.databaseMigrator

import application.databaseMigrator.Migrator
import org.gradle.api.tasks.TaskAction

public class Validate extends Migrator {
    @TaskAction
    public void run() {
        initMigrator().validate()
        logger.quiet('Schema Validated !')
    }
}
