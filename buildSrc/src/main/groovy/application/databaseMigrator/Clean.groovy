package application.databaseMigrator

import application.databaseMigrator.Migrator
import org.gradle.api.tasks.TaskAction

public class Clean extends Migrator {
    @TaskAction
    public void run() {
        initMigrator().clean()
        logger.quiet('Schema Cleaned !')
    }
}

