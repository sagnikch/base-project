package application.databaseMigrator

import org.gradle.api.Project
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.options.Option
import org.gradle.api.InvalidUserDataException
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.configuration.ClassicConfiguration

public class Migrator extends DefaultTask {
    private static final ERROR_CONNECTION_URL = "Connection url to database is required"
    private static final ERROR_CONNECTION_USER = "Connection user for database is required"
    private static final ERROR_CONNECTION_PASSWORD = "Connection password for database is required"
    private static final SCHEMA_HISTORY_TABLE = 'schema_history'
    private static final MIGRATOR_ENCODING = "UTF-8"

    private String connection_url = System.getenv('DB_URL')
    private String connection_user = System.getenv('DB_USER')
    private String connection_password = System.getenv('DB_PASSWORD')
    private String migration_dir = ({
        System.getenv('DB_MIGRATION_DIR') ?: ("filesystem:" + project.projectDir.toString() + "/src/main/resources/db/migration")
    }())

    def validity = { attr, message ->
        if(!attr) {
          throw new InvalidUserDataException(message)
        }
    }

    @Option(option = "db_url", description = "Configures the url to the database [required]")
    private void setConnectionUrl(String url) {
        this.connection_url = url
    }

    @Option(option = "db_user", description = "Configures the user for the database [required]")
    private void setConnectionUser(String user) {
        this.connection_user = user
    }

    @Option(option = "db_password", description = "Configures the password for the database [required]")
    private void setConnectionPassword(String password) {
        this.connection_password = password
    }

    @Option(option = "migration_dir", description = "Configures the directory containing migrations")
    private void setMigrationDir(String migration_dir) {
        this.migration_dir = migration_dir
    }

    private void checkParamValidity() {
        validity(connection_url, ERROR_CONNECTION_URL)
        validity(connection_user, ERROR_CONNECTION_USER)
        validity(connection_password, ERROR_CONNECTION_PASSWORD)
    }

    public Flyway initMigrator() {
        checkParamValidity()
        return Flyway.configure()
                     .encoding(MIGRATOR_ENCODING)
                     .locations(migration_dir)
                     .table(SCHEMA_HISTORY_TABLE)
                     .dataSource(connection_url, connection_user, connection_password)
                     .load()
    }
}
