package application.databaseMigrator

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.options.Option
import org.gradle.api.tasks.options.OptionValues
import org.gradle.api.InvalidUserDataException
import org.gradle.api.tasks.TaskAction

public class FileGenerator extends DefaultTask {
    private static enum MigrationType {
        V, R
    }
    private static final ERROR_MIGRATION_TYPE = 'Invalid migration type'
    private static final ERROR_FILE_NAME_FORMAT = 'Invalid file name'
    private static final SQL_FILE_DATA = '-- input sql here'
    private static String path = 'src/main/resources/db/migration'

    private String file_name = 'migration'
    private MigrationType migration_type = MigrationType.V

    def validate_file_name_format = { file_name ->
        if(!(file_name =~ /\A[a-zA-Z_]*\z/)) {
            throw new InvalidUserDataException(ERROR_FILE_NAME_FORMAT)
        }
    }
    def version = { (new Date()).getTime() }
    def file_path_name = { path, file_name -> sprintf("%s/%s", path, file_name) }
    def r_migration_type_format = { file_name -> sprintf("R__%s.sql", file_name) }
    def v_migration_type_format = { file_name, version -> sprintf("V%s__%s.sql", version, file_name) }
    def create_file_with_data = { file_name, data -> (new File(file_name)).text = data }

    @Option(option = 'name', description = 'Configures the name of the file generated')
    private void setFileName(String file_name) {
        this.file_name = file_name
    }

    @Option(option = 'type', description = 'Configures the type of migration')
    private void setMigrationType(MigrationType migration_type) {
        this.migration_type = migration_type
    }

    @OptionValues('type')
    public List<MigrationType> getAvailableMigrationTypes() {
        return new ArrayList<MigrationType>(Arrays.asList(MigrationType.values()))
    }

    private void checkParamValidity() {
        validate_file_name_format(file_name)
    }

    @TaskAction
    public void generateMigration() {
        checkParamValidity()
        String file_path
        switch(migration_type) {
            case MigrationType.R:
                file_path = file_path_name(path, r_migration_type_format(file_name))
                break
            case MigrationType.V:
                file_path = file_path_name(path, v_migration_type_format(file_name, version()))
                break
            default:
                throw new InvalidUserDataException(ERROR_MIGRATION_TYPE)
        }
        create_file_with_data(file_path, SQL_FILE_DATA)
        logger.quiet('Generated File\n\t# migration type - {}\n\t# file path - {}', migration_type, file_path)
    }
}
