package application.databaseMigrator

import application.databaseMigrator.Migrator
import org.gradle.api.tasks.options.Option
import org.gradle.api.tasks.TaskAction

public class Migrate extends Migrator {
    @TaskAction
    public void run() {
        def migrated_count = initMigrator().migrate()
        logger.quiet('Schema Migrated !\n\t# successful count - {}', migrated_count)
    }
}
