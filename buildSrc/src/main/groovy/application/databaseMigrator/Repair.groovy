package application.databaseMigrator

import application.databaseMigrator.Migrator
import org.gradle.api.tasks.TaskAction

public class Repair extends Migrator {
    @TaskAction
    public void run() {
        initMigrator().repair()
        logger.quiet('Schema Repaired !')
    }
}
