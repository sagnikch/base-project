package application

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.options.Option
import org.gradle.api.InvalidUserDataException
import io.swagger.codegen.DefaultGenerator
import io.swagger.codegen.config.CodegenConfigurator

class SwaggerCodeGenerator extends DefaultTask {
    public static final String DEFAULT_OUTPUT_DIR = '/temp/rest_client_stubs/output'
    public static final String ERROR_EMPTY_LANGUAGE = 'language is a required parameter!'
    public static final String ERROR_EMPTY_TARGET = 'target to specification [url/file_path] is required!'

    private String language
    private String target
    private String output_directory = ({ project.projectDir.toString() + DEFAULT_OUTPUT_DIR }())

    def validity = { attr, message ->
        if(!attr) {
          throw new InvalidUserDataException(message)
        }
    }

    @Option(option = 'language', description = 'Configures the language in which client stub is to be produced [required]')
    def setLanguage(String language) {
        this.language = language
    }

    @Option(option = 'target', description = 'Specifies the path[url/file_path] to Swagger API Specification [required]')
    def setTarget(String target) {
        this.target = target
    }

    @Option(option = 'output_directory', description = 'Configures the output directory where generated client ' +
                                                       'stubs would be put, default - ' + DEFAULT_OUTPUT_DIR)
    def setOutputDirectory(String output_directory) {
        this.output_directory = output_directory
    }

    private void checkParamValidity() {
        validity(language, ERROR_EMPTY_LANGUAGE)
        validity(target, ERROR_EMPTY_TARGET)
    }

    @TaskAction
    def generateClientStub() {
        checkParamValidity()
        CodegenConfigurator configurator = new CodegenConfigurator()
        configurator.setLang(language)
                    .setInputSpec(target)
                    .setOutputDir(output_directory)
        (new DefaultGenerator()).opts(configurator.toClientOptInput())
                                .generate()
        logger.quiet('Directory Generated !')
    }
}

