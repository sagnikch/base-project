package application

import org.gradle.api.InvalidUserDataException
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.api.Project
import spock.lang.Specification
import application.SwaggerCodeGenerator

class SwaggerCodeGeneratorSpec extends Specification {
    Project project = ProjectBuilder.builder().build()
    def temp_dir = 'src/test/temp/'
    def task = project.task('generateClientStub', type: SwaggerCodeGenerator )
    def sample_language = 'java'
    def default_output_directory = project.projectDir.toString() + SwaggerCodeGenerator.DEFAULT_OUTPUT_DIR
    def sample_output_directory = sprintf("%s/%s", temp_dir, "swagger_stub")
    def sample_swagger_spec_file_path = 'src/test/fixtures/SwaggerSampleFile.json'

    def 'when valid language, target, output_dir are provided - then generate an output directory'() {
        setup:
            task.setLanguage(sample_language)
            task.setTarget(sample_swagger_spec_file_path)
            task.setOutputDirectory(sample_output_directory)
        when:
            task.generateClientStub()
        then:
            (new File(sample_output_directory)).length() > 0
        cleanup:
            (new File(sample_output_directory)).deleteDir()
    }

    def 'when valid language and target are provided - then output generated to default directory'() {
        setup:
            task.setLanguage(sample_language)
            task.setTarget(sample_swagger_spec_file_path)
        when:
            task.generateClientStub()
        then:
            (new File(default_output_directory)).length() > 0
        cleanup:
            (new File(default_output_directory)).deleteDir()
    }

    def 'when target is not provided - then exception must be thrown'() {
        setup:
            task.setLanguage(sample_language)
        when:
            task.generateClientStub()
        then:
            final InvalidUserDataException exception = thrown()
            exception.message == 'target to specification [url/file_path] is required!'
    }

    def 'when language is not provided - then exception must be thrown'() {
        setup:
            task.setTarget(sample_swagger_spec_file_path)
        when:
            task.generateClientStub()
        then:
            final InvalidUserDataException exception = thrown()
            exception.message == 'language is a required parameter!'
    }
}

