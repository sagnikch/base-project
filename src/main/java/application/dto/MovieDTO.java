package application.dto;

import application.model.Movie;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Setter
@ToString
public class MovieDTO implements Serializable {

    private Long id;
    private String title;
    private String year;
    private String director;
    public enum Genre {
        ACTION, ANIMATION, COMEDY, HORROR, SCIFI, ROMANCE
    }
    @Enumerated(EnumType.STRING)
    private MovieDTO.Genre genre;
}
