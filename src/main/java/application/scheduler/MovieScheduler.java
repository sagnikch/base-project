package application.scheduler;

import application.job.QuartzSampleJob;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.quartz.JobDetail;
import org.quartz.Trigger;


public class MovieScheduler extends BaseScheduler {

  public static final Map<JobDetail, Set<? extends Trigger>> map =  new HashMap<>();

  static {
    cron1();
  }

  private static void cron1() {
    Set<Trigger> trigger = new HashSet<Trigger>();
    trigger.add(createCronTrigger("0 12 09 ? * *", "Trigger1", "group1").build());
    map.put(createJobDetail(QuartzSampleJob.class, "jobDetail1", "group1").build(), trigger);
  }
}

