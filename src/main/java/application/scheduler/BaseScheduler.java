package application.scheduler;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.TriggerBuilder;

abstract class BaseScheduler {
  public static JobBuilder createJobDetail(Class jobClass, String groupName, String name) {
    return JobBuilder.newJob(jobClass)
                      .withIdentity(name, groupName)
                      .storeDurably(true)
                      .requestRecovery(true);
  }

  public static TriggerBuilder createCronTrigger(
                                                  String cronExpression, 
                                                  String name, 
                                                  String group
  ) {
    return TriggerBuilder.newTrigger()
                          .withIdentity(name, group)
                          .withPriority(1)
                          .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression)
                                        .withMisfireHandlingInstructionFireAndProceed());
  }
}

