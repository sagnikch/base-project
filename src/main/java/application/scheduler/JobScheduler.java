package application.scheduler;

import org.quartz.Scheduler;

public class JobScheduler {
  public static void schedule(Scheduler scheduler) throws Exception {
    scheduler.scheduleJobs(MovieScheduler.map, true);
  }
}

