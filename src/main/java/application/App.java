package application;

import application.config.OpenCensusMetricExporterConfig;
import application.lib.NetworkTracer;
import io.sentry.Sentry;

import java.io.IOException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.HandlerExceptionResolver;

@SpringBootApplication
public class App {
  @Bean
  public HandlerExceptionResolver sentryExceptionResolver() {
    return new io.sentry.spring.SentryExceptionResolver();
  }

  @Bean
  public ServletContextInitializer sentryServletContextInitializer() {
    return new io.sentry.spring.SentryServletContextInitializer();
  }

  public static void main(String[] args) {
    /*
    Please set the environment variable "SENTRY_DSN" to activate Traceback reporting to Sentry
    example:
    export SENTRY_DSN="https://changme@sentry.io/changeme"
    */
    try {
      NetworkTracer.init();
      Sentry.init();
      OpenCensusMetricExporterConfig.setupOpenCensusAndPrometheusExporter();
      SpringApplication.run(App.class, args);
    } catch (IOException e) {
      System.err.println("Failed to create and register OpenCensus Prometheus Stats exporter " + e);
      Sentry.capture(e);
    } catch (Exception e) {
      Sentry.capture(e);
    }
  }
}
