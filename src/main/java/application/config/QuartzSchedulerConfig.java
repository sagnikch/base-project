package application.config;

import application.scheduler.JobScheduler;
import java.io.IOException;
import java.util.Properties;
import javax.sql.DataSource;
import org.quartz.Scheduler;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

@Configuration
@ConditionalOnProperty(name = "quartz.enabled")
public class QuartzSchedulerConfig {

  @Bean
  public JobFactory jobFactory(ApplicationContext applicationContext) {
    AutoWiringSpringBeanJobFactory jobFactory = new AutoWiringSpringBeanJobFactory();
    jobFactory.setApplicationContext(applicationContext);
    return jobFactory;
  }

  @Bean
  public Scheduler schedulerFactoryBean(DataSource datasource, JobFactory jobFactory
                                        ) throws Exception {
    SchedulerFactoryBean factory = new SchedulerFactoryBean();
    factory.setOverwriteExistingJobs(true);
    factory.setDataSource(datasource);
    factory.setJobFactory(jobFactory);
    factory.setQuartzProperties(quartzProperties());
    factory.afterPropertiesSet();
    Scheduler scheduler = factory.getScheduler();
    scheduler.setJobFactory(jobFactory);
    JobScheduler.schedule(scheduler);
    scheduler.start();
    return scheduler;
  }

  @Bean
  public Properties quartzProperties() throws IOException {
    YamlPropertiesFactoryBean propertiesFactoryBean = new YamlPropertiesFactoryBean();
    propertiesFactoryBean.setResources(new ClassPathResource("/quartz.yml"));
    propertiesFactoryBean.afterPropertiesSet();
    return propertiesFactoryBean.getObject();
  }
}

