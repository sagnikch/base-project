package application.config;

import java.util.Arrays;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
  public static final List<ResponseMessage> globalResponses = Arrays.asList(
                        (new ResponseMessageBuilder())
                          .code(200)
                          .message("OK")
                          .build(),
                        (new ResponseMessageBuilder())
                          .code(400)
                          .message("Bad Request")
                          .build(),
                        (new ResponseMessageBuilder())
                          .code(401)
                          .message("Unauthorized")
                          .build(),
                        (new ResponseMessageBuilder())
                          .code(404)
                          .message("Not Found")
                          .build(),
                        (new ResponseMessageBuilder())
                          .code(500)
                          .message("Internal Server Error")
                          .build()
  );

  @Bean
  public Docket apiV1() {
    return (new Docket(DocumentationType.SWAGGER_2))
        .groupName("MovieAPIVersion1")
        .useDefaultResponseMessages(false)
        .globalResponseMessage(RequestMethod.GET, globalResponses)
        .globalResponseMessage(RequestMethod.POST, globalResponses)
        .globalResponseMessage(RequestMethod.PUT, globalResponses)
        .globalResponseMessage(RequestMethod.PATCH, globalResponses)
        .globalResponseMessage(RequestMethod.DELETE, globalResponses)
        .select()
        .apis(RequestHandlerSelectors.basePackage("application.controllers.api.v1"))
        .paths(PathSelectors.any())
        .build().apiInfo(apiEndPointsInfo());
  }

  private ApiInfo apiEndPointsInfo() {
    return (new ApiInfoBuilder())
          .title("REST APIs Documentation")
          .description("Documentation for RESTful APIs present in project")
          .version("1.0")
          .build();
  }
}
