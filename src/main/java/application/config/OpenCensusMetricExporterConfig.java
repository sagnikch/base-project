package application.config;

import application.services.opencensus.metrics.RegisterViewsService;
import io.opencensus.exporter.stats.prometheus.PrometheusStatsCollector;
import io.prometheus.client.exporter.HTTPServer;
import java.io.IOException;

public class OpenCensusMetricExporterConfig {   
  private static String host = System.getenv("PE_HOST") != null 
                                ? System.getenv("PE_HOST") : 
                                  "0.0.0.0";
  private static int port = System.getenv("PE_PORT") != null
                              ? Integer.decode(System.getenv("PE_PORT")) : 
                              8889;

  public static void setupOpenCensusAndPrometheusExporter() throws IOException {
    RegisterViewsService.registerAllViews();
    PrometheusStatsCollector.createAndRegister();
    new HTTPServer(host, port, true);
  }
}
