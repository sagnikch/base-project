package application.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JavaUtility {
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  public static <T> JsonNode getJsonNode(T object) {
    return OBJECT_MAPPER.convertValue(object, JsonNode.class);
  }
}

