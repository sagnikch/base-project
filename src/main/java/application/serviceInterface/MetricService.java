package application.serviceInterface;

import io.opencensus.common.Scope;
import io.opencensus.stats.Aggregation;
import io.opencensus.stats.Aggregation.Distribution;
import io.opencensus.stats.BucketBoundaries;
import io.opencensus.stats.Measure.MeasureDouble;
import io.opencensus.stats.Measure.MeasureLong;
import io.opencensus.stats.MeasureMap;
import io.opencensus.stats.Stats;
import io.opencensus.stats.StatsRecorder;
import io.opencensus.stats.View;
import io.opencensus.stats.View.Name;
import io.opencensus.stats.ViewManager;
import io.opencensus.tags.TagContext;
import io.opencensus.tags.TagContextBuilder;
import io.opencensus.tags.TagKey;
import io.opencensus.tags.TagMetadata;
import io.opencensus.tags.TagMetadata.TagTtl;
import io.opencensus.tags.TagValue;
import io.opencensus.tags.Tagger;
import io.opencensus.tags.Tags;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public abstract class MetricService {
  public static final MeasureDouble DOUBLE = MeasureDouble
                                                  .create(
                                                      "latency", 
                                                      "The latency in milliseconds per request",
                                                      "ms"
                                                  );
  public static final MeasureLong LONG = MeasureLong
                                              .create("count",
                                                  "count the number of requests",
                                                  "1"
                                              );
  public static final TagKey KEY_API = TagKey.create("api");
  public static final TagKey KEY_TYPE = TagKey.create("type");
  public static final TagKey KEY_DESCRIPTION = TagKey.create("description");
  private static final TagTtl METADATA = TagTtl.NO_PROPAGATION;
  private static final StatsRecorder statsRecorder = Stats.getStatsRecorder();
  private static final Aggregation LATENCY_DISTRIBUTION = Distribution
        .create(BucketBoundaries
          .create(
              Arrays.asList(
                  0.0, 2.0, 50.0, 75.0, 100.0, 200.0, 
                  400.0, 600.0, 800.0, 1000.0, 2000.0, 
                  4000.0, 6000.0
              )
  ));    
  private static final Aggregation COUNT_AGGREGATION = Aggregation.Count.create();  
  private static final List<TagKey> TAGKEYS  = Arrays.asList(KEY_API, KEY_TYPE, KEY_DESCRIPTION);

  private static void recordTaggedStat(List<TagKey> keys, String[] values, 
                                      Object measure, Number d) {
    Tagger tagger = Tags.getTagger();
    TagContextBuilder builder = tagger.emptyBuilder();

    for (int i = 0; i < keys.size(); i++) {
      builder.put(keys.get(i), TagValue.create(values[i]), TagMetadata.create(METADATA));
    }
    TagContext tctx = builder.build();
    try (Scope ss = tagger.withTagContext(tctx)) {
      MeasureMap map =  statsRecorder.newMeasureMap();
      if (measure == DOUBLE) {
        map.put(DOUBLE, d.doubleValue()).record();
      } else {
        map.put(LONG, d.longValue()).record();
      }
    }
  }

  private static Number measureInMS(long startTimeMs) {
    return (System.currentTimeMillis() - startTimeMs) / 1.0;
  }

  public final static void recordMetrics(String api, String type, String description, MeasureDouble measure, long value) {
    String[] tagValues = {api, type, description};
    recordTaggedStat(TAGKEYS, tagValues, measure, measureInMS(value));
  }

  public final static void recordMetrics(String api, String type, String description, MeasureLong measure) {
    String[] tagValues = {api, type, description};
    recordTaggedStat(TAGKEYS, tagValues, measure, 1);
  }

  public static void registerAllViews(String latencyView, String countAggregationView) {
    View[] views = new View[] {
      View.create(Name.create(latencyView), 
                  "Latency Distribution", 
                  DOUBLE, LATENCY_DISTRIBUTION,
                  TAGKEYS),
      View.create(Name.create(countAggregationView), 
                  "Counts the number of requests based"
                  + "on KEY_TYPE", 
                  LONG, COUNT_AGGREGATION, TAGKEYS),
            View.create(Name.create("meettrriccss"),
                    "Counts the number of requests based"
                            + "on KEY_TYPE",
                    LONG, COUNT_AGGREGATION, TAGKEYS),
    };

    ViewManager vmgr = Stats.getViewManager();
    for (View view : views) {
      vmgr.registerView(view);
    }
  }
}

