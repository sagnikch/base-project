package application.validator;

import application.utils.JavaUtility;
import com.fasterxml.jackson.databind.JsonNode;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.ValidationMessage;
import java.util.Map;
import java.util.Set;

public class ProductControllerValidator extends Validator {
  public static final JsonSchema SCHEMA_PRODUCT;
  public static final JsonSchema SCHEMA_PRODUCT_ID;
  
  static {
    try {
      SCHEMA_PRODUCT = getSchema("/productController/product.json");
      SCHEMA_PRODUCT_ID = getSchema("/productController/productId.json");
    } catch (Exception e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  private static JsonSchema getSchema(String path) {
    return FACTORY.getSchema(ProductControllerValidator.class
      .getResourceAsStream(BASE_PATH + path));
  }

  public static final Map<String, JsonSchema> jsonFileMap = Map.of(
                                                              "object", SCHEMA_PRODUCT,
                                                              "id", SCHEMA_PRODUCT_ID
                                                            );
  
  public static <T> Set<ValidationMessage> validate(T object, String schemaType) {
    JsonNode jsonNode = JavaUtility.getJsonNode(object);
    return jsonFileMap.get(schemaType).validate(jsonNode);
  }
}

