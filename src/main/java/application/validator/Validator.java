package application.validator;

import com.networknt.schema.JsonSchemaFactory;

abstract class Validator {
  public static final JsonSchemaFactory FACTORY = JsonSchemaFactory.getInstance();
  public static final String BASE_PATH = "/api/v1/schema";
}
