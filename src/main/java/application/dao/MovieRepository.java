package application.dao;

import application.model.Movie;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

    @Query(value="SELECT * FROM movies WHERE title = ?1", nativeQuery = true)
    Optional<Movie> findMovieByTitle(@Param("title") String title);

    
}
