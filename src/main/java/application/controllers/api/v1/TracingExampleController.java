package application.controllers.api.v1;

import application.lib.NetworkTracer;
import io.opencensus.common.Scope;
import io.opencensus.trace.Status;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/trace_example")
public class TracingExampleController {

  @GetMapping(path = "/with_sync_http_call", produces = "text/plain")
  public @ResponseBody String withSyncHttpCall() {
    try (Scope scope = NetworkTracer.startScopedSpan("sync.http.call")) {
      HttpRequest.Builder requestBuilder = NetworkTracer
                                           .httpRequestBuilder()
                                           .uri(URI.create("http://127.0.0.1:8081/trace_example/third_party_service"))
                                           .version(HttpClient.Version.HTTP_1_1)
                                           .GET();
      HttpResponse<String> response = HttpClient
                                      .newHttpClient()
                                      .send(requestBuilder.build(), BodyHandlers.ofString());
      NetworkTracer.addAnnotation("Synchronously Calling External Service A", "http_response", response.body());
    } catch (Exception e) {
      System.out.println("ErrorOccured: " + e);
    }
    return "OK";
  }

  @GetMapping(path = "/with_async_http_call", produces = "text/plain")
  public @ResponseBody String withAsyncHttpCall() {
    try (Scope scope = NetworkTracer.startScopedSpan("async.http.call")) {
      HttpRequest.Builder requestBuilder = NetworkTracer
                                           .httpRequestBuilder()
                                           .uri(URI.create("http://127.0.0.1:8081/trace_example/third_party_service"))
                                           .version(HttpClient.Version.HTTP_1_1)
                                           .GET();
      HttpClient
      .newHttpClient()
      .sendAsync(requestBuilder.build(), BodyHandlers.ofString())
      .thenApply(HttpResponse::body)
      .thenAccept(response -> {
        NetworkTracer.addAnnotation("Asynchronously Calling External Service A",
                                    "http_response",
                                    response.toString());
      });
    } catch (Exception e) {
      System.out.println("ErrorOccured: " + e);
    }
    return "OK";
  }

  @GetMapping(path = "/with_exception", produces = "text/plain")
  public @ResponseBody String withException() {
    try (Scope scope = NetworkTracer.startScopedSpan("exception.occuring.action")) {
      NetworkTracer.addAnnotation("Method Raises Exxception", "server.error", "500");
      try {
        throw new UnsupportedOperationException("You shouldn't call this method!");
      } catch (Exception e) {
        NetworkTracer.setStatus(Status.INTERNAL.withDescription(e.toString()));
      }
    } catch (Exception e) {
      System.out.println("ErrorOccured: " + e);
    }
    return "OK";
  }

  @GetMapping(path = "/third_party_service", produces = "text/plain")
  public @ResponseBody String thirdPartyService() {
    try (Scope scope = NetworkTracer.startScopedSpan("third.party")) {
      Thread.sleep(2000);
      NetworkTracer.addAnnotation("Delayed Response", "service.health", "weak");
    } catch (Exception e) {
      System.out.println("ErrorOccured: " + e);
    }
    return "OK";
  }
}
