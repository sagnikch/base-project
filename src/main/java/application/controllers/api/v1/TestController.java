package application.controllers.api.v1;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/test")
public class TestController {

  @GetMapping(path = "/ping", produces = "text/plain")
  public @ResponseBody String ping() {
    return "PONG";
  }

}
