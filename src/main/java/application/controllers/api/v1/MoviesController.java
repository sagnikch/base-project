package application.controllers.api.v1;

import application.controllers.api.exceptions.ResourceNotFoundException;
import application.dao.MovieRepository;
import application.model.Movie;
import application.dto.MovieDTO;
import application.services.MovieService;
import application.services.opencensus.metrics.MovieMetricService;
import io.sentry.Sentry;
import io.sentry.event.BreadcrumbBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("v1")
@RequestMapping("/api/v1/movies")
@Api(description = "RESTful endpoints for resource [Movie]")
public class MoviesController {

  private static final String ENTITY_NAME = "movie";

  @Autowired
  private MovieRepository movieRepository;

  @Autowired
  private MovieService movieService;

  private final Logger log = LoggerFactory.getLogger(MoviesController.class);

  @Autowired
  MovieRepository movies;

  void unsafeMethod() {
    throw new UnsupportedOperationException("You shouldn't call this!");
  }

  @ApiOperation(value = "Creates a new Movie object")
  @PostMapping(produces = "application/json")
  public MovieDTO createMovie(@Valid @RequestBody MovieDTO movieDTO) {
        long startTimeMS = System.currentTimeMillis();
        //movieDTO = movies.save(movieDTO);
        log.debug("REST request to save Customer : {}", movieDTO);
        MovieDTO result = movieService.save(movieDTO);

    MovieMetricService.recordMetrics("movie_api", "genre", 
                                      movieDTO.getGenre().toString(), 
                                      MovieMetricService.LONG);
    
    MovieMetricService.recordMetrics("movie_api", "api", "latency", 
            MovieMetricService.DOUBLE, startTimeMS
);
        return result;
// return ResponseEntity
// .created(new URI("/api/customers/" + result.getId()))
// .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
// .body(result);
  }

  @ApiOperation(value = "List of all Movie objects")
  @GetMapping(produces = "application/json")
  public List<Movie> getAllMovies() {
    long startTimeMS = System.currentTimeMillis();
    // Record a breadcrumb in the context.
    Sentry.getContext().recordBreadcrumb(
          new BreadcrumbBuilder().setMessage("Get all Movie objects").build()
    );
    Sentry.getContext().addTag("api", "getAllMovies");
    //unsafeMethod(); 
    List<Movie> movieList = movies.findAll();
    MovieMetricService.recordMetrics("movie_api", "api", "latency", 
                                          MovieMetricService.DOUBLE, startTimeMS
    );
    return movieList;
  }

  @ApiOperation(value = "Retrieves a Movie object by its {id}")
  @GetMapping(value = "id/{id}", produces = "application/json")
  @ApiParam(value = "id", required = true)
  public Movie getMovieById(@PathVariable(value = "id") Long movieId) {
    long startTimeMS = System.currentTimeMillis();
    Movie movie = movies.findById(movieId)
                      .orElseThrow(
            () -> new ResourceNotFoundException("Movie Not Found ! MOVIE ID #" + movieId)
                      );  
    MovieMetricService.recordMetrics("movie_api", "api", "latency", 
                                          MovieMetricService.DOUBLE, startTimeMS
    );
    MovieMetricService.recordMetrics("movie_api", "id", movieId.toString(), 
                                          MovieMetricService.LONG );
    return movie;
  }

  @ApiOperation(value = "Retrieves a Movie object by its {id} and updates its state")
  @PutMapping(value = "/{id}", produces = "application/json")
  @ApiParam(value = "id", required = true)
  public Movie updateMovie(@PathVariable(value = "id") Long movieId,
                              @RequestBody Movie movieState) {
    long startTimeMS = System.currentTimeMillis();
    Movie movie = movies.findById(movieId)
                              .orElseThrow(
          () -> new ResourceNotFoundException("Movie Not Found ! MOVIE ID #" + movieId)
                              );
    movie.setTitle(
        movieState.getTitle() != null ? movieState.getTitle() : movie.getTitle()
    );
    movie.setYear(
        movieState.getYear() != null ? movieState.getYear() : movie.getYear()
    );
    movie.setGenre(
        movieState.getGenre() != null ? movieState.getGenre() : movie.getGenre()
    );
    movie.setDirector(
        movieState.getDirector() != null ? movieState.getDirector() : movie.getDirector()
    );
    movie =  movies.save(movie);
    MovieMetricService.recordMetrics("movie_api", "id", movieId.toString(), 
                                          MovieMetricService.LONG
    );
    MovieMetricService.recordMetrics("movie_api", "api", "latency",
                                        MovieMetricService.DOUBLE, startTimeMS
    );
    return movie;
  }

  @ApiOperation(value = "Retrieves a Movie object by its {title}")
  @GetMapping(value = "title/{title}", produces = "application/json")
  @ApiParam(value = "title", required = true)
  public Movie getMovieByTitle(@PathVariable(value = "title") String title) {
    long startTimeMS = System.currentTimeMillis();
    Movie movie = movies.findMovieByTitle(title)
                      .orElseThrow(
            () -> new ResourceNotFoundException("Movie Not Found ! MOVIE title #" + title)
                      );  
    MovieMetricService.recordMetrics("movie_api", "api", "latency", 
                                          MovieMetricService.DOUBLE, startTimeMS
    );
    MovieMetricService.recordMetrics("movie_api", "id", title, 
                                          MovieMetricService.LONG );
    return movie;
  }

  @ApiOperation(value = "Deletes the Movie object identified by {id}")
  @DeleteMapping(value = "/{id}", produces = "text/plain")
  @ApiParam(value = "id", required = true)
  public void deleteMovie(@PathVariable(value = "id") Long movieId) {
    long startTimeMS = System.currentTimeMillis();
    movies.deleteById(movieId);
    MovieMetricService.recordMetrics("movie_api", "id", movieId.toString(), 
                                          MovieMetricService.LONG
    );
    MovieMetricService.recordMetrics("movie_api", "api", "latency", 
                                          MovieMetricService.DOUBLE, startTimeMS
    ); 
  }
}
