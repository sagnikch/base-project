package application.controllers.api.v1;

import application.validator.ProductControllerValidator;
import com.fasterxml.jackson.databind.JsonNode;
import com.networknt.schema.ValidationMessage;
import java.util.Set;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/v1/products")
public class ProductController {
  @PostMapping(produces = "text/plain")
  ResponseEntity<String> validateProduct(@RequestBody JsonNode product) {
    Set<ValidationMessage> errors = ProductControllerValidator.validate(product, "object");
    if (errors.size() > 0) {
      return new ResponseEntity<String>(errors.toString(), HttpStatus.BAD_GATEWAY);
    }
    return new ResponseEntity<String>("Valid !", HttpStatus.OK);
  }

  @GetMapping(path = "/{id}")
  ResponseEntity<String> validateProductId(@PathVariable(value = "id") Long productId) {
    Set<ValidationMessage> errors = ProductControllerValidator.validate(productId, "id");
    if (errors.size() > 0) {
      return new ResponseEntity<String>(errors.toString(), HttpStatus.BAD_GATEWAY);
    }
    return new ResponseEntity<String>("Valid !", HttpStatus.OK);
  }
}
