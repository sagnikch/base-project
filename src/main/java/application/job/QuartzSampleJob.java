package application.job;

import application.services.QuartzSampleService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

public class QuartzSampleJob implements Job {
  @Autowired
  private QuartzSampleService service;

  @Override
  public void execute(JobExecutionContext jobExecutionContext) {
    service.countMovies();
  }    
}