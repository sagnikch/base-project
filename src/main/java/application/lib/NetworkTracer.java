package application.lib;

import io.opencensus.common.Scope;
import io.opencensus.exporter.trace.jaeger.JaegerTraceExporter;
import io.opencensus.exporter.trace.jaeger.JaegerExporterConfiguration;
import io.opencensus.trace.Tracer;
import io.opencensus.trace.Status;
import io.opencensus.trace.Span;
import io.opencensus.trace.AttributeValue;
import io.opencensus.trace.Tracing;
import io.opencensus.trace.SpanContext;
import io.opencensus.trace.propagation.SpanContextParseException;
import io.opencensus.trace.config.TraceConfig;
import io.opencensus.trace.config.TraceParams;
import io.opencensus.trace.propagation.TextFormat;
import javax.servlet.http.HttpServletRequest;
import io.opencensus.trace.samplers.Samplers;
import org.springframework.stereotype.Component;
import java.net.http.HttpRequest;
import java.util.HashMap;

@Component
public class NetworkTracer {
  public static final Tracer tracer = Tracing.getTracer();
  public static final TextFormat textFormat = Tracing.getPropagationComponent()
                                                     .getB3Format();
  
  public static final TextFormat.Getter<HttpServletRequest> getter = 
    new TextFormat.Getter<HttpServletRequest>() { 
      @Override
      public String get(HttpServletRequest httpRequest, String s) {
        return httpRequest.getHeader(s);
      }
    };

  public static final TextFormat.Setter<HttpRequest.Builder> setter =
    new TextFormat.Setter<HttpRequest.Builder>() {
      public void put(HttpRequest.Builder carrier, String key, String value) {
        carrier.setHeader(key, value);
      }
    };

  public static String ExporterUrl = (
    System.getenv("NT_EXPORTER_URL") != null ? System.getenv("NT_EXPORTER_URL") : "http://127.0.0.1:14268/api/traces"
  );
  public static String ServiceName = (
    System.getenv("SERVICE_IDENTIFIER") != null ? System.getenv("SERVICE_IDENTIFIER") : "default"
  );

  public static void init() {
    JaegerTraceExporter.createAndRegister(
      JaegerExporterConfiguration
      .builder()
      .setThriftEndpoint(ExporterUrl)
      .setServiceName(ServiceName)
      .build()
    );
    TraceConfig traceConfig = Tracing.getTraceConfig();
    TraceParams activeTraceParams = traceConfig.getActiveTraceParams();
    traceConfig.updateActiveTraceParams(
      activeTraceParams
      .toBuilder()
      .setSampler(Samplers.alwaysSample())
      .build()
    );
  }

  public static Scope startScopedSpan(HttpServletRequest request, String tag) {
    try {
      SpanContext spanContext = textFormat.extract(request, getter);
      return tracer
             .spanBuilderWithRemoteParent(tag, spanContext)
             .startScopedSpan();
    }
    catch (SpanContextParseException e) {
      return tracer.spanBuilder(tag).startScopedSpan();
    }  
  }

  public static Scope startScopedSpan(String tag) {
    return tracer.spanBuilder(tag).startScopedSpan();
  }

  public static String traceID() {
    return tracer
           .getCurrentSpan()
           .getContext()
           .getTraceId()
           .toString();
  }

  public static SpanContext currentContext() {
    return tracer
           .getCurrentSpan()
           .getContext();
  }

  public static Span currentSpan(){
    return tracer.getCurrentSpan();
  }

  public static void setStatus(Status status){
    tracer.getCurrentSpan().setStatus(status);;
  }

  public static HttpRequest.Builder httpRequestBuilder() {
    HttpRequest.Builder requestBuilder = HttpRequest.newBuilder();
    textFormat.inject(currentContext(), requestBuilder, setter);
    return requestBuilder;
  }

  public static void addAnnotation(String description, String key, String value) {
    HashMap <String, AttributeValue> attribute = new HashMap<String, AttributeValue>();
    attribute.put(key, AttributeValue.stringAttributeValue(value));
    tracer.getCurrentSpan().addAnnotation(description, attribute);
  }

  public static void addAnnotations(String description, HashMap<String, String> attributes) {
    HashMap <String, AttributeValue> annotationAttributes = new HashMap<String, AttributeValue>();
    attributes.forEach((key, value) ->
      annotationAttributes.put(key, AttributeValue.stringAttributeValue(value))
    ); 
    tracer.getCurrentSpan().addAnnotation(description, annotationAttributes);
  }

  public static void shutdown() {
    Tracing.getExportComponent().shutdown();
  }
}