package application.lib.filters;

import application.lib.NetworkTracer;
import io.opencensus.common.Scope;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class HttpRequestFilter extends OncePerRequestFilter {
  @Override
  public void doFilterInternal(HttpServletRequest request,
                               HttpServletResponse response,
                               FilterChain filterchain) 
      throws IOException, ServletException {
    try(Scope scope = NetworkTracer.startScopedSpan(request, "request")) {
      filterchain.doFilter(request, response);
    }
  }
}
