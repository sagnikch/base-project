package application.services;

import org.springframework.stereotype.Service;
 
@Service
public class TestService {
    public String ping() {
        return "PONG";
    }
}
