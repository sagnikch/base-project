package application.services.opencensus.metrics;

import org.springframework.stereotype.Service;

@Service
public class RegisterViewsService {

  public static void registerAllViews() {
    MovieMetricService.registerViews();
  }
}

