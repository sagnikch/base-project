package application.services.opencensus.metrics;

import application.serviceInterface.MetricService;
import org.springframework.stereotype.Service;

@Service
public class MovieMetricService extends MetricService {
  public static void registerViews() {
    registerAllViews("movie_api/latency", "movie_api/count_aggregation");
  }   
}

