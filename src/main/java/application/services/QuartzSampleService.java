package application.services;

import application.dao.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
@Service
public class QuartzSampleService {

  @Autowired
  MovieRepository movies;

  public void countMovies() {
    System.out.println("Total number of movies are: " + movies.count());
  }
}
