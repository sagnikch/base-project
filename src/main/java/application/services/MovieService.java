package application.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import application.dao.MovieRepository;
import application.model.Movie;
import application.dto.MovieDTO;
import org.modelmapper.ModelMapper;

@Service
public class MovieService {
    
    private final Logger log = LoggerFactory.getLogger(MovieService.class);

    private static final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private MovieRepository movieRepository;

    protected MovieService() {}

    public MovieDTO save(MovieDTO movieDTO) {
        log.debug("Request to save Movie : {}", movieDTO);
        Movie movie = new Movie();
        movie.setDirector(movieDTO.getDirector());
        movie.setGenre(Movie.Genre.ACTION);
        movie.setTitle(movieDTO.getTitle());
        movie.setYear(movieDTO.getYear());
        movie = movieRepository.save(movie);
        MovieDTO result = new MovieDTO();
        result.setId(movie.getId());
        result.setDirector(movie.getDirector() != null ? movie.getDirector() : movieDTO.getDirector());
        result.setTitle(movie.getTitle() != null ? movie.getTitle() : movieDTO.getTitle());
        result.setYear(movie.getYear() != null ? movie.getYear() : movieDTO.getYear());
        return result;
    }

}
