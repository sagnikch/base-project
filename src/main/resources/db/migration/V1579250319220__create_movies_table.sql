CREATE TABLE movies (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `title` varchar(255) NOT NULL,
    `year` varchar(255) NOT NULL,
    genre varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
);

