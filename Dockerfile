# Installing gradle, so that it can be cached as a layer
FROM adoptopenjdk:11-jdk-hotspot-bionic AS getGradle
CMD ["gradle"]

ENV GRADLE_HOME /opt/gradle

RUN set -o errexit -o nounset \
    && echo "Adding gradle user and group" \
    && groupadd --system --gid 1000 gradle \
    && useradd --system --gid gradle --uid 1000 --shell /bin/bash --create-home gradle \
    && mkdir /home/gradle/.gradle \
    && chown --recursive gradle:gradle /home/gradle \
    \
    && echo "Symlinking root Gradle cache to gradle Gradle cache" \
    && ln -s /home/gradle/.gradle /root/.gradle

VOLUME /home/gradle/.gradle
WORKDIR /home/gradle

RUN apt-get update \
    && apt-get install --yes --no-install-recommends \
        fontconfig \
        unzip \
        wget \
        \
        bzr \
        git \
        git-lfs \
        mercurial \
        openssh-client \
        subversion \
    && rm -rf /var/lib/apt/lists/*

ENV GRADLE_VERSION 6.3
ARG GRADLE_DOWNLOAD_SHA256=038794feef1f4745c6347107b6726279d1c824f3fc634b60f86ace1e9fbd1768
RUN set -o errexit -o nounset \
    && echo "Downloading Gradle" \
    && wget --no-verbose --output-document=gradle.zip "https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip" \
    \
    && echo "Checking download hash" \
    && echo "${GRADLE_DOWNLOAD_SHA256} *gradle.zip" | sha256sum --check - \
    \
    && echo "Installing Gradle" \
    && unzip gradle.zip \
    && rm gradle.zip \
    && mv "gradle-${GRADLE_VERSION}" "${GRADLE_HOME}/" \
    && ln --symbolic "${GRADLE_HOME}/bin/gradle" /usr/bin/gradle \
    \
    && echo "Testing Gradle installation" \
    && gradle --version

FROM adoptopenjdk:11-jdk-hotspot-bionic AS appBuild
ENV APP_HOME=/opt/app
RUN mkdir -p $APP_HOME/src/main/java
COPY --from=getGradle . .
WORKDIR ${APP_HOME}

# The following two steps speed up the build by creating a cache with the dependencies downloaded
COPY --chown=gradle:gradle build.gradle \
     gradle.properties\
     settings.gradle $APP_HOME/
COPY --chown=gradle:gradle config $APP_HOME/config
COPY --chown=gradle:gradle buildSrc $APP_HOME/buildSrc
COPY --chown=gradle:gradle gradle $APP_HOME/gradle
RUN gradle build -x check -x test --info

# Copying src and running final build
COPY --chown=gradle:gradle src $APP_HOME/src
RUN gradle build -x check -x test --info

FROM adoptopenjdk:11-jre-hotspot
EXPOSE 8080
RUN mkdir /application
COPY --from=appBuild /opt/app/build/libs/*.jar /application/spring-boot-application.jar
ENTRYPOINT ["java", \
            "-Djava.security.egd=file:/dev/./urandom","-jar",\
            "-Dlog4j2.contextSelector=org.apache.logging.log4j.core.async.AsyncLoggerContextSelector",\
            "-Dsentry.stacktrace.app.packages=application",\
            "/application/spring-boot-application.jar"]
