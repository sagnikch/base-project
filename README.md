
# Service Starter Kit [Spring Boot]

[![pipeline status](https://gitlab.example.in/arjun.verma/spring_boot/badges/master/pipeline.svg)](https://gitlab.example.in/arjun.verma/spring_boot/commits/master)
[![coverage report](https://gitlab.example.in/arjun.verma/spring_boot/badges/master/coverage.svg)](https://gitlab.example.in/arjun.verma/spring_boot/commits/master)

Service Starter Kit is an effort to provide a boiler-plate for consolidated functionalities required, when venturing into the Micro-service Architecture.

## Table of Contents

- [Getting Started](#getting-started)

  - [Prerequisites](#prerequisites)
  - [Configuration](#configuration)
  - [Installing](#prerequisites)
  - [Vesioning](#versioning)
  - [Running Tests](#running-tests)
   - [Deployment](#deployment)
   - [CI/CD](#ci/cd)
- [Documentation](#documentation)
   - [Using Swagger](#using-swagger)
   - [Building with Docker](#building-with-docker)
   - [Database Version Control with Flyway](#database-version-control-with-flyway)
   - [Sentry for Exception Monitoring](#sentry-for-exception-monitoring)
   - [OpenCensus for App Metric and Network Tracing](#opencensus-for-app-metric-and-network-tracing)
   - [JsonSchema for API Validation](#jsonschema-for-api-validation)
   - [Using Spring Boot Actuator for health check, static metric data, etc](#using-spring-boot-actuator-for-health-check,-static-metric-data,-etc)
   - [Using Quartz for Scheduled Jobs](#using-quartz-for-scheduled-jobs)



## Getting Started

### **Prerequisite**

Install Java Version 11

### **Configuration**

We follow the [12 Factor App Methodology](https://12factor.net/). Hence, application configuration that change with respect to the deployment environment will be driven through **ENVIRONMENT VARIABLES**.

**Variables**

env variable | default value | required | usage
-------------|---------------|-----------|---
DB_URL| - | Yes | configures the connection url to database
DB_USER| - | Yes | configures user for connection to database 
DB_PASSWORD| - | Yes | configures password for connection to database
ENFORCE_COVERAGE| 0 | No | enforces build to fail if test code coverage does not meet the required criteria of 90%
SERVICE_IDENTIFIER| "default" | No | set an identifier for your service
NT_EXPORTER_URL| http://127.0.0.1:14268/api/traces | No | sets the network traces aggregator url, i.e where network traces are to be sent
SERVICE_IDENTIFIER| "default" | No | set an identifier for your service
SENTRY_DSN| - | No | sets the destination url for sentry server
PE_HOST| 0.0.0.0 | No | sets the host to which prometheus exporter agent can accumulate metric data sent from the application by the opencensus client agent
PE_HOST| 8889 | No | sets the port for the same as above

### **Installing**

The following steps will get you up and running in no time.

- clone service starter kit

   ```#!/bin/bash
      git clone git@gitlab.example.in:tech_arch/starter_kit_java.git
   ```

- run all pending database migrations

   * set the database url, database user & database password environment variables in your terminal session
      ```
      export DB_URL="jdbc:mysql://localhost:3306/database"
      export DB_USER="username"
      export DB_PASSWORD="password"
      ```
   * run pending database migrations
      ```
      gradle dbMigrate
      ```
* start application server

   ```
   gradle clean build bootRun
   ```
### **Functionalities**

   The following are basic properties that any particular service starter kit should have . We have selected these libraries from a pool of other libraries based on our POCs.

   **MVC**

   feature | library
   -------------|---------------
   Framework| Spring Boot
   ORM| Hibernate
   Database Connection Pooling| HikariCP
   Logging| log4j2
   Embedded App Server| Undertow

   **Code Quality**

   feature | library
   -------------|---------------
   Static Code Analyser| PMD
   Style Check & Linting| Checkstyle
   Code Coverage| Jacoco

   **Monitoring**

   feature | library
   --------|--------
   Exception Monitoring| Sentry
   Network Tracing| Opencensus[client], Jaeger[aggregator]
   Application Business Metric| Opencensus[client], Prometheus[aggregator]
   Application Static Metric| Actuator with - Prometheus, Dynatrace
   Application Performance Monitoring| Dynatrace

   **Documentation**

   feature | library
   --------|--------
   API Documentation| Swagger 2.0

   **Other**

   feature | library
   --------|--------
   CI| Gitlab CI
   Version Control For Database| Gradle tasks provided. See [Documentation](#documentation) for more
   Docker Image Creator| Gradle tasks provided. See [Documentation](#documentation) for more

### **Versioning**

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.example.in/arjun.verma/spring_boot/tags).

### **Running tests**

```
# to run all tests
gradle test

# specific class
gradle test --tests org.gradle.SomeTestClass

# specific class and method
gradle test --tests org.gradle.SomeTestClass.someSpecificMethod

# method name containing spaces
gradle test --tests "org.gradle.SomeTestClass.some method containing spaces"

# all classes at specific package (recursively)
gradle test --tests 'all.in.specific.package*'

# specific method at specific package (recursively)
gradle test --tests 'all.in.specific.package*.someSpecificMethod'

gradle test --tests '*IntegTest'

gradle test --tests '*IntegTest*ui*'

gradle test --tests '*ParameterizedTest.foo*'

# the second iteration of a parameterized test
gradle test --tests '*ParameterizedTest.*[2]'
```

### **Deployment**
*to be added*

### **CI/CD**
*to be added*

## **Documentation**

### **Using Swagger**
   [Swagger™](https://swagger.io/) is a project used to > describe and document RESTful APIs.
   The Swagger specification (*OpenAPI specification*) defines a set of files required to describe such an API.
   These files can then be used by the Swagger-UI project to display the API and Swagger-Codegen to generate clients in various languages.

   **Provided**
   1. **Swagger UI**:
      Generates the interactive documentation for users to quickly learn about your API.
      You can find api documentation at `{host}/swagger-ui.html#` and json representation for your APIs at `{host}/v2/api-docs?group={group_name}`.

   1. **Generating REST client stubs**:
      [Codegen](https://swagger.io/tools/swagger-codegen/) simplifies the build process by generating server stubs and client SDKs for any API, defined with the *OpenAPI specification*. We have provided this functionality by creating a Gradle task that wraps around the *Codegen* library to generate client stubs.

      * Run Task
         * ```
            gradle generateClientStub --language="<language>" --target="{host}/v2/api-docs"
           ```
      * Task Info
         * ```
            gradle help --task generateClientStub
           ```
      * Task Details
         * **--language** | Programming language in which you want to generate the client stub
         * **--target** | Path to json file/ url to location, where OpenAPI specification for your API is present
         * **--output_directory** | Output directory path for the generated client stub

### **Building with Docker**
[Docker](https://docs.docker.com/engine/docker-overview/) is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so that you can deliver software quickly.

   **Provided**
   1. **Generating Docker container**: We have provided this functionality by creating a Gradle task that creates a Docker container of your application
      * Run task
         * ```
            gradle dockerBuild -P imageName="spring_release:1.0.0"
           ```
      * Task Info
         * ```
            gradle help --task dockerBuild
           ```
      * Task Details
         * **--imageName** | Configures the name of the Docker image that is to be built

### **Database Version Control with Flyway**
   [Flyway](https://flywaydb.org/documentation/) is an open-source database migration tool. It strongly favors simplicity and convention over configuration. We have created *Gradle wrapper tasks* for every functionality that Flyway provides.
      
   **Provided**
   * **Database Migration**: Migrates pending migrations.
      * command - `gradle dbMigrate` 
   * **Database Info**: Provides information regarding database migrations.
      * command - `gradle dbInfo`
   * **Database Validate**: Validates database w.r.t migrations.
      * command - `gradle dbValidate`
   * **Database Clean**: Drops all tables from the database.
      * command - `gradle dbClean`
   * **Database Repair**: Repairs the database w.r.t migrations.
      * command - `gradle dbRepair`



### **Sentry for Exception Monitoring**
[Sentry](https://sentry.io/) is a service that helps you monitor and fix crashes in realtime.

* Sentry is disabled by default - Environment variable `SENTRY_DSN` needs to be set to enable Sentry.
  *  `export SENTRY_DSN="https://changme@sentry.io/changeme"`
* The shutdown timeout for Sentry reporting is disabled by default. i.e If a crash causes the application to stop, Sentry threads are terminated immediately without waiting for the existing tracebacks to be sent.
  * This can be controlled by setting the Environment variable `SENTRY_ASYNC_SHUTDOWNTIMEOUT`
   *  `export SENTRY_ASYNC_SHUTDOWNTIMEOUT=5000`

### **OpenCensus for App Metric and Network Tracing**
[OpenCensus](https://opencensus.io/) is a set of libraries for various languages that allow you to collect application metrics and distributed traces, then transfer the data to a backend of your choice in real time.
   
   **Provided**
   1. **OpenCensus metrics with prometheus**

      Application and request metrics are important indicators of availability. We have used [Prometheus](https://prometheus.io/docs/) as a metrics backend that provides system monitoring and alerting toolkit.

      **Prerequisite**

         * Install and Run Prometheus Server
         
            ```
            docker run \
            -p 9090:9090 \
            -v /{proj_dir}/prometheus.yml:/etc/prometheus/prometheus.yml \
            prom/prometheus
            ```
            Save prometheus.example as a file named prometheus.yml in your project directory.

      **Recording Metrics**
      
      *MetricService*
      * Create quantitative metrics that we will record 
      * Create tags that we will associate with our metrics

      Views are used to organize our metrics. Views are specific to the api so it has to be defined in a separate service which is extended from the MetricService.

      You can use the following function to record the metrics:
      ```
      {service_name}.recordMetrics(api_name, type, description, measure, measure_value)
      ```
      where,

         * **api_name:** name of the api
         * **type:** key to filter or group the metrics
         * **description:** The value that gives meaning to the key
         * **measure:** metric type defined in MetricService
         * **measure_value:** value associated with measure

### **JsonSchema for API Validation**

   [JsonSchema](https://json-schema.org/) is a declarative language for validating the format and structure of a JSON Object. It allows us to specify the number of special primitives to describe exactly what a valid JSON Object will look like.
  
  **Provided**

   *Validation*

   1. *Validator Class*

      Abstract class that can be extended by other validator classes.

      validator classes should define the following:
         * **JsonSchema variable** 
         * **validate function:** 
            * arguments: object that needs to be validated, 
            * return type: Set[ValidationMessage] 

      Refer to ProductControllerValidator for implementation details.


   2. *Schema*

      Define Json schema corresponding to the JSON Object that needs to be validated in resources folder.
   
      Base path relative to reosurces folder is: '/api/v1/schema'

### **Using Spring Boot Actuator for health check, static metric data, etc**
   Spring boot’s module [Actuator](https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-features.html#production-ready) allows you to monitor and manage application usages in production environment.

   These monitoring and management information is exposed via REST like endpoint URLs.

   **Provided**

   1. **Endpoints:**

   By default, all endpoints are prefixed with /actuator.
   
   Each [Endpoint](https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-features.html#production-ready-endpoints) can be enabled or disabled.

   * **Enabled endpoints:** 
      * /: Provides list of all endpoints exposed by actuators
      * /metrics: Provides list of all metrics
      * /health: Shows application health information
      * /prometheus: Provides prometheus scrape with the appropriate format

   * To enable the endpoint, include the endpoint in application.yml as:
      ```
         management.endpoints.web.exposure.include = {endpoint_name}
      ```
      where, the include property lists the IDs of the endpoints that are exposed.
   2.  **Backend:** [Prometheus](https://prometheus.io/docs/)

### **Using Quartz for Scheduled Jobs**
   [Quartz](http://www.quartz-scheduler.org/)is a richly featured, open source job scheduling library that can be integrated within virtually any Java application - from the smallest stand-alone application to the largest e-commerce system. 
   
   Quartz can be used to create simple or complex schedules for executing tens, hundreds, or even tens-of-thousands of jobs; jobs whose tasks are defined as standard Java components that may execute virtually anything you may program them to do.
   
   The Quartz Scheduler includes many enterprise-class features, such as support for JTA transactions and clustering


   **Provided**

   **Configuration Files**

   * *QuartzSchedulerConfig*
   * *quartz.yml*

   **Scheduler Classes**

   * *BaseScheduler*

      Provides the method to build Triggers and JobDetails.
  
   * *JobScheduler*

      In  order to schedule jobs, schedulers have to register the jobdetails and triggers in the schedule method of this class.

## **Acknowledgments**
* [12 Factor App](https://12factor.net/)
