package application

import spock.lang.Specification
import application.services.TestService;

class TestServiceSpec extends Specification {
    def 'should successfully reply with pong'() {
        setup:
            def service = new TestService()
        when:
            def result = service.ping()
        then:
            result == "PONG"
    }
}
